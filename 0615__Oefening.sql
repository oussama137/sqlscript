use modernways;
alter view auteursboeken as
select concat( personen.Voornaam,' ', personen.Familienaam ) as Auteur, Titel, boeken.Id as Boeken_Id
from boeken inner join publicaties on boeken.Id = publicaties.Boeken_Id
inner join personen on publicaties.Personen_Id = personen.Id;