use modernways;
CREATE VIEW AuteursBoeken
AS
SELECT concat(Voornaam,' ', Familienaam) as auteur, titel 
FROM (personen
INNER JOIN publicaties
ON personen_Id = personen.id)
inner join boeken 
on Boeken_Id = boeken.id;