use modernways;
create view GemiddeldeRatings as
select reviews.Boeken_Id, avg(reviews.Rating) as Rating
from reviews group by reviews.Boeken_Id;