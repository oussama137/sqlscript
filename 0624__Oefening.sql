use aptunes;
SELECT Titel, Naam, Lengte FROM Liedjes
INNER JOIN Bands
ON Liedjes.Bands_Id = Bands.Id
WHERE Titel LIKE 'A%'
ORDER BY Lengte;

CREATE INDEX TitelLengteIdx
ON liedjes(titel, lengte);