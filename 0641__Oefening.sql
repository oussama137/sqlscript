use aptunes;
delimiter $$
create procedure GetLiedjes(in term varchar(50))
begin
select Titel
from Liedjes
where Titel like concat('%',term,'%');
end$$
delimiter ;