use aptunes;
SELECT Titel, Naam
FROM Liedjesgenres INNER JOIN Liedjes
ON Liedjesgenres.Liedjes_Id = Liedjes.Id
INNER JOIN Genres
ON Liedjesgenres.Genres_Id = Genres.Id;

CREATE INDEX naamIdx
ON genres(Naam);