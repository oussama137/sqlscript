use modernways;
create view AuteursBoekenRatings as
select Auteur, Titel, Rating
from auteursboeken inner join gemiddelderatings on auteursboeken.Boeken_Id = gemiddelderatings.Boeken_Id;