use aptunes;
SELECT Voornaam, Familienaam, count(Lidmaatschappen.Muzikanten_Id)
FROM Muzikanten INNER JOIN Lidmaatschappen
ON Lidmaatschappen.Muzikanten_Id = Muzikanten.Id
GROUP BY Familienaam, Voornaam
ORDER BY Voornaam, Familienaam;


CREATE INDEX FamilienaamVoornaam
ON muzikanten(familienaam, voornaam);