SELECT Games.Titel, Platformen.Naam
FROM Games 
LEFT JOIN Platformen 
INNER JOIN Releases 
ON Releases.Platformen_Id = Platformen.Id
ON Releases.Games_Id = Games.Id;
