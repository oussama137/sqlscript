use aptunes;

delimiter $$
create procedure CreateAndReleaseAlbum(in titel varchar(100),in bands_Id int)
begin
start transaction;
insert into Albums(Titel)
values 
(titel);
insert into Albumreleases (Bands_Id,Albums_id)
values 
(Bands_Id,Last_insert_id());
commit;
end$$
delimiter ;